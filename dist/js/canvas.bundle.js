/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/canvas.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/canvas.js":
/*!**************************!*\
  !*** ./src/js/canvas.js ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var canvas = document.querySelector('canvas');
var c = canvas.getContext('2d');

canvas.width = innerWidth;
canvas.height = innerHeight;

var mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2

    // Event Listeners
};addEventListener('mousemove', function (event) {
    mouse.x = event.clientX;
    mouse.y = event.clientY;
});

addEventListener('resize', function () {
    canvas.width = innerWidth;
    canvas.height = innerHeight;

    init();
});

// Objects
function Particle(radius, velocity, radians, size, drag) {
    var _this = this;

    this.radius = radius;
    this.radians = radians;
    this.velocity = velocity;
    this.size = size;
    this.drag = drag;
    this.update = function () {
        _this.draw();
        _this.radians += _this.velocity;
    };
    this.draw = function () {
        c.lineCap = "butt";
        c.moveTo(mouse.x + Math.cos(_this.radians) * _this.radius, mouse.y + Math.sin(_this.radians) * _this.radius);
        c.beginPath();
        c.arc(mouse.x, mouse.y, _this.radius, _this.drag + _this.radians, _this.radians, true);
        c.lineWidth = _this.size;
        c.strokeStyle = "#FFF";
        c.stroke();
    };
}

// Implementation
var objects = void 0;
function init() {
    c.rect(0, 0, canvas.width, canvas.height);
    c.fillStyle = 'rgba(0,0,0,0.5)';
    c.fill();
    objects = [];
    objects.push(new Particle(100, 0.073, 1, 2, +16 * Math.PI / 16));
    objects.push(new Particle(90, 0.062, 2, 0.8, +1 * Math.PI / 16));
    objects.push(new Particle(85, 0.091, 3, 1.2, +5 * Math.PI / 16));
    objects.push(new Particle(80, 0.081, 4, 1.6, +9 * Math.PI / 16));
    objects.push(new Particle(75, 0.062, 5, 2, +12 * Math.PI / 16));
    objects.push(new Particle(70, 0.042, 6, 1.2, +7 * Math.PI / 16));
    objects.push(new Particle(65, 0.021, 7, 1.6, +4 * Math.PI / 16));
    objects.push(new Particle(60, 0.061, 8, 0.6, +2 * Math.PI / 16));
    objects.push(new Particle(55, 0.1, 9, 1.1, +9 * Math.PI / 16));
    objects.push(new Particle(50, 0.09, 10, 0.1, +19 * Math.PI / 16));
    objects.push(new Particle(45, 0.051, 11, 1.4, +25 * Math.PI / 16));
    objects.push(new Particle(40, 0.041, 12, 0.4, +13 * Math.PI / 16));
    objects.push(new Particle(35, 0.13, 13, 1.6, +22 * Math.PI / 16));
    objects.push(new Particle(30, 0.063, 14, 0.6, +9 * Math.PI / 16));
}

// Animation Loop
function animate() {
    requestAnimationFrame(animate);
    c.rect(0, 0, canvas.width, canvas.height);
    c.fillStyle = 'rgba(0,0,0,0.07)';
    c.fill();
    objects.forEach(function (object) {
        object.update();
    });
}

init();
animate();

/***/ })

/******/ });
//# sourceMappingURL=canvas.bundle.js.map