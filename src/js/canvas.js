const canvas = document.querySelector('canvas')
const c = canvas.getContext('2d')

canvas.width = innerWidth
canvas.height = innerHeight

const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2
}

// Event Listeners
addEventListener('mousemove', event => {
    mouse.x = event.clientX
    mouse.y = event.clientY
})

addEventListener('resize', () => {
    canvas.width = innerWidth
    canvas.height = innerHeight

    init()
})

// Objects
function Particle(radius, velocity, radians, size, drag) {
    this.radius = radius
    this.radians = radians
    this.velocity = velocity
    this.size = size
    this.drag = drag
    this.update = () => {
        this.draw()
        this.radians += this.velocity
    }
    this.draw = () => {
        c.lineCap = "butt";
        c.moveTo(mouse.x + Math.cos(this.radians)*this.radius, mouse.y + Math.sin(this.radians)*this.radius)
        c.beginPath()
        c.arc(mouse.x, mouse.y, this.radius, this.drag + this.radians, this.radians, true)
        c.lineWidth = this.size
        c.strokeStyle = "#FFF"
        c.stroke()
    }
}

// Implementation
let objects
function init() {
    c.rect(0, 0, canvas.width, canvas.height)
    c.fillStyle = 'rgba(0,0,0,0.5)'
    c.fill()
    objects = []
    objects.push(new Particle(100, 0.073, 1, 2, + 16 * Math.PI/16))
    objects.push(new Particle(90, 0.062, 2, 0.8, + 1 * Math.PI/16))
    objects.push(new Particle(85, 0.091, 3, 1.2, + 5 * Math.PI/16))
    objects.push(new Particle(80, 0.081, 4, 1.6, + 9 * Math.PI/16))
    objects.push(new Particle(75, 0.062, 5, 2, + 12 * Math.PI/16))
    objects.push(new Particle(70, 0.042, 6, 1.2, + 7 * Math.PI/16))
    objects.push(new Particle(65, 0.021, 7, 1.6, + 4 * Math.PI/16))
    objects.push(new Particle(60, 0.061, 8, 0.6, + 2 * Math.PI/16))
    objects.push(new Particle(55, 0.1, 9, 1.1, + 9 * Math.PI/16))
    objects.push(new Particle(50, 0.09, 10, 0.1, + 19 * Math.PI/16))
    objects.push(new Particle(45, 0.051, 11, 1.4, + 25 * Math.PI/16))
    objects.push(new Particle(40, 0.041, 12, 0.4, + 13 * Math.PI/16))
    objects.push(new Particle(35, 0.13, 13, 1.6, + 22 * Math.PI/16))
    objects.push(new Particle(30, 0.063, 14, 0.6, + 9 * Math.PI/16))

}

// Animation Loop
function animate() {
    requestAnimationFrame(animate)
    c.rect(0, 0, canvas.width, canvas.height)
    c.fillStyle = 'rgba(0,0,0,0.07)'
    c.fill()
    objects.forEach(object => {
     object.update()
    })
}

init()
animate()
